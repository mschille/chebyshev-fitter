/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file KahanSum.h
 *
 * @brief A simple Kahan summation class.
 *
 * @author Manuel Schiller
 * @date 2024-02-27
 */
#pragma once

#include <type_traits>

/** @brief Kahan summation.
 *
 * Kahan summation is a compensated summation algorithm, i.e. it accumulates
 * roundoff in a carry variable, and folds it back into the sum with each
 * added term at the cost of doing four additions/subtractions per added term
 * instead of just one.
 *
 * Here's how to use the class:
 * @code
 * float compensated_sum(const std::vector<float>& input)
 * {
 *     float sum = 0, carry = 0;
 *     KahanSum ksum{sum, carry};
 *     for (const auto& el: input)
 *         ksum += el;
 *     return sum;
 * }
 * @endcode
 *
 * @note The compensation algorithm uses the fact that floating point addition
 * is not associative. Aggessive compiler optimization (specifically when the
 * compiler assumes that floating point addition is associative) have been
 * known to break the compensation in this class. Steer clear of -ffast-math
 * or -fassociative-math (which is turned on by -ffast-math) flags.
 *
 * Fortunately, it is easy to check if the compiler's optimizer breaks the
 * compensated summation:
 * @code
 * constexpr eps_half = std::numeric_limits<float>::epsilon() / 2;
 * float s = 0, c = 0;
 * KahanSum<float> ksum{s, c};
 * ksum += 1.f;
 * ksum += eps_half;
 * ksum += 1.f;
 * if (0.f == s) {
 *     std::fputs("Compiler optimization broke KahanSum class.\n", stderr);
 *     std::terminate();
 * }
 * @endcode
 *
 * @author Manuel Schiller
 * @date 2024-02-27
 */
template <class T, class = std::enable_if_t<std::is_floating_point_v<T>>>
class KahanSum final {
private:
    T& m_sum;
    T& m_carry;
public:
    /// construct a KahanSum object from sum and carry variables
    constexpr KahanSum(T& sum, T& carry) noexcept : m_sum(sum), m_carry(carry)
    {}

    /// add a number to the sum (compensated addition)
    constexpr KahanSum& operator+=(const T& x) noexcept
    {
        const T y = x - m_carry;
        const T t = m_sum + y;
        const T u = t - m_sum;
        m_carry = u - y;
        m_sum = t;
        return *this;
    }

    /// subtract a number from the sum (compensated subtraction)
    constexpr KahanSum& operator-=(const T& x) noexcept
    {
        const T y = x + m_carry;
        const T t = m_sum - y;
        const T u = t - m_sum;
        m_carry = u + y;
        m_sum = t;
        return *this;
    }
};

// vim: tw=78:sw=4:ft=cpp:et
