/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */

/** @file ChebyshevIterator.h
 *
 * @brief simple Chebyshev iterator class
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-03-24
 */
#pragma once

#include <cmath>

/// Chebyshev polynomials of first, second, third or fourth kind
enum class Kind { First, Second, Third, Fourth };

/** @brief evaluate Chebyshev polynomials of increasing orders at given x
 *
 * @tparam T    floating point type for calculations
 * @tparam KIND which kind of Chebyshev polynomial to evaluate
 *
 * @note Moving from one order to the next (i.e. calling operator++)
 * essentially costs one FMA (fused multiply-add) if your hardware supports
 * it, or one multiply and one add.
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2019-03-24
 */
template <typename T, Kind KIND>
class ChebyshevIterator {
    private:
        T m_last = 1;
        T m_twox = 0;
        T m_curr = 0;

        template <class F>
        constexpr static F fma(F x, F y, F z) noexcept
        {
#ifdef FP_FAST_FMAF
            constexpr bool has_fast_fmaf = true;
#else // !defined(FP_FAST_FMAF)
            constexpr bool has_fast_fmaf = false;
#endif // defined(FP_FAST_FMAF)
#ifdef FP_FAST_FMA
            constexpr bool has_fast_fma = true;
#else // !defined(FP_FAST_FMA)
            constexpr bool has_fast_fma = false;
#endif // defined(FP_FAST_FMA)
#ifdef FP_FAST_FMAL
            constexpr bool has_fast_fmal = true;
#else // !defined(FP_FAST_FMAL)
            constexpr bool has_fast_fmal = false;
#endif // defined(FP_FAST_FMAL)
            if constexpr ((std::is_same_v<float, F> && has_fast_fmaf) ||
                          (std::is_same_v<double, F> && has_fast_fma) ||
                          (std::is_same_v<double, F> && has_fast_fmal)) {
                // if there's a specialised FMA for F, assume it's in scope,
                // or found via argument-dependent lookup (ADL)
                using std::fma;
                return fma(x, y, z);
            } else {
                // quick and dirty implementation for machines which either
                // lack an FMA, or lack FMA that's faster than the pedestrian
                // version below (note: you do not get the advantages of
                // increased precision with this...)
                return x * y + z;
            }
        }

    public:
        /// construct from given x in [-1, 1]
        constexpr explicit ChebyshevIterator(const T& x) noexcept
                : m_twox{2 * x},
                  m_curr{(2 - (KIND == Kind::First)) * x -
                         (KIND == Kind::Third) + (KIND == Kind::Fourth)}
        {}

        /// get current value of Chebyshev polynomial at current order
        [[nodiscard]] constexpr T operator*() const noexcept
        {
            return m_last;
        }

        /// move on to next order, return reference to new value
        constexpr ChebyshevIterator& operator++() noexcept
        {
            const T next = ChebyshevIterator::fma(m_twox, m_curr, -m_last);
            m_last = m_curr;
            m_curr = next;
            return *this;
        }

        /// move on to next order, return copy of new value
        [[nodiscard]] constexpr ChebyshevIterator operator++(int) noexcept
        {
            auto retVal{*this};
            operator++();
            return retVal;
        }
};

/// type used to pass minimum and maximum values along a dimension
template <class T>
struct minmax_t {
    T min;
    T max;
};

/// type to do the transformation of coordinates
template <class T>
class transform_t {
    private:
        T mid;    ///< mid-point of interval
        T iwidth; ///< 2 / (width of interval)

    public:
        /// construct from minimum and maximum
        constexpr explicit transform_t(const minmax_t<T>& minmax) noexcept
            : mid{(minmax.max + minmax.min) / T(2)},
            iwidth{T(2) / (minmax.max - minmax.min)}
        {}
        /// transform from x in [min, max] to x' in [-1, +1]
        constexpr T operator()(const T& x) const noexcept
        { return iwidth * (x - mid); }
};

// test that transform_t is correct (assumes binary floating point types)
static_assert(-1.f == transform_t<float>{{-2.f, 2.f}}(-2.f),
              "transform_t is broken");
static_assert(-.5f == transform_t<float>{{-2.f, 2.f}}(-1.f),
              "transform_t is broken");
static_assert(0.f == transform_t<float>{{-2.f, 2.f}}(0.f),
              "transform_t is broken");
static_assert(.5f == transform_t<float>{{-2.f, 2.f}}(1.f),
              "transform_t is broken");
static_assert(1.f == transform_t<float>{{-2.f, 2.f}}(2.f),
              "transform_t is broken");

// vim: sw=4:tw=78:ft=cpp:et
