/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file Test.h
 *
 * @brief trivially simple unit test class
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-15
 */
#pragma once

#include <cstdio>
#include <initializer_list>

/** brief ultra-simple testing framework
 *
 * Here's how to define a test:
 * @code
 * bool test_add2(void)
 * {
 *     Test test(__func__);
 *     int x = 40;
 *     if (42 != (x + 2)) fail(test);
 *     pass(test);
 * }
 * @endcode
 *
 * The routine returns true if the test passes, false otherwise. For each test
 * executed, a line is written to stdout, indicating test name and status,
 * and, if the test fails, when file and line number it failed in. Note that
 * pass and fail are implemented as C preprocessor macros (see below).
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-15
 */
class Test {
private:
    unsigned m_line = 0;
    const char* m_file = nullptr;
    const char* m_name;
    bool m_ok = true;
public:
    explicit Test(const char* name) noexcept : m_name{name} {}
    ~Test() noexcept
    {
        std::printf("%-32s: ", m_name);
        if (m_ok) {
            std::printf("PASSED\n");
        } else {
            std::printf("FAILED (in %s:%u)\n", m_file, m_line);
        }
    }

    constexpr operator bool() const noexcept { return m_ok; }

    Test& fail(const char* file, unsigned line) noexcept
    {
        m_file = file;
        m_line = line;
        m_ok = false;
        return *this;
    }
};

/// fail the test t (preprocessor macro)
#define fail(t) do { return (t).fail(__FILE__, __LINE__); } while (false)
/// pass the test t (preprocessor macro)
#define pass(t) do { return (t); } while (false)

/// type of function used for unit tests
using test_t = bool (*)(void);

/// run a suite of tests, return non-zero if any test fails
template <class T = std::initializer_list<test_t>>
int run_test_suite(const char* name, T&& tests)
{
    std::printf("%s: running tests\n", name);
    std::size_t ntotal = 0, nfail = 0;
    for (const auto& test: tests) {
        ++ntotal;
        nfail += !test();
    }
    if (!nfail) {
        std::printf("%s: ALL %zu TESTS PASSED\n", name, ntotal);
    } else {
        std::printf("%s: %zu OUT OF %zu TESTS (%.3f%%) FAILED\n", name,
                    nfail, ntotal, 100.f * nfail / ntotal);
    }

    return nfail ? 1 : 0;
}

// vim: tw=78:sw=4:ft=cpp:et
