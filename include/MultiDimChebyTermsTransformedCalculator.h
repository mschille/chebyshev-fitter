/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file MultiDimChebyTermsTransformedCalculator.h
 *
 * @brief calc. (transformed) terms in a multi-dim. Chebyshev up to given order
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-19
 */
#pragma once

#include "MultiDimChebyTermsCalculator.h"

/** @brief calculate terms of Chebyshev polynomials (1st kind) up to MAXORDERS
 *
 * This class is exactly like MultiDimChebyTermsCalculator, but transforms the
 * coordinates given to operator() prior to use from the intervals [min, max]
 * given during construction (one per coordinate in point) to the standard
 * intervals [-1, +1] used by MultiDimChebyTermsCalculator internally.
 *
 * @tparam T            floating point type used for calculations
 * @tparam MAXORDERS    max. order to which to calculate terms per dimension
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date   2024-02-19
 */
template <class T, std::size_t... MAXORDERS>
class MultiDimChebyTermsTransformedCalculator
        : public MultiDimChebyTermsCalculator<T, MAXORDERS...> {
public:
    using typename MultiDimChebyTermsCalculator<T, MAXORDERS...>::point_t;
    using typename MultiDimChebyTermsCalculator<T, MAXORDERS...>::value_type;
    using minmax_t = ::minmax_t<T>;

private:
    using transform_t = ::transform_t<T>;

    /// transformations of interval for each dimension
    std::array<transform_t, sizeof...(MAXORDERS)> m_transforms;

    template <std::size_t... IDXs>
    constexpr point_t
    transform_intervals(std::index_sequence<IDXs...> /* unused */,
                        const point_t& p) const noexcept
    {
        return {(m_transforms[IDXs](p[IDXs]))...};
    }

public:
    template <
            class... ARGS,
            class = std::enable_if_t<sizeof...(ARGS) == sizeof...(MAXORDERS)>,
            class = std::enable_if_t<
                    ((std::is_same_v<minmax_t,
                                     std::remove_cv_t<std::remove_reference_t<
                                             ARGS>>>) &&
                     ...)>>
    MultiDimChebyTermsTransformedCalculator(ARGS&&... minmaxes)
            : MultiDimChebyTermsCalculator<T, MAXORDERS...>{},
              m_transforms{transform_t{std::forward<ARGS>(minmaxes)}...}
    {}

    value_type operator()(const point_t& p) noexcept
    {
        return MultiDimChebyTermsCalculator<T, MAXORDERS...>::operator()(
                transform_intervals(
                        std::make_index_sequence<sizeof...(MAXORDERS)>{}, p));
    }
};

// vim: tw=78:sw=4:ft=cpp:et
