/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file include/ChebyshevFitter.h
 *
 * @brief A fitter for multidimensional Chebyshev approximations
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-03-03
 */
#pragma once

#include <array>
#include <cstdio>
#include <numeric>
#include <stdexcept>
#include <sys/stat.h> // mkdir
#include <vector>

#include "ChebyshevIterator.h"
#include "MultiDimChebyTermsTransformedCalculator.h"
#include "LinModelFitter.h"
#include "MultiIndex.h"
#include "sfmt.h"

#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>

/** @brief A fitter for multidimensional Chebyshev approximations
 *
 * @tparam T            data type in which to do calculations (double)
 * @tparam USEKAHANSUM  normally false, set to true if you have especially
 *                      difficult fit where you run into a near-singular
 *                      matrix; this option uses Kahan summation to accumulate
 *                      matrix and right hand side to avoid some of the
 *                      numerical cancellation issues by keeping track of the
 *                      roundoff, and adding it back into the sum
 * @tparam NIN          number of input variables per measurement
 * @tparam NOUT         number of outputs to fit (i.e. this class runs NOUT
 *                      fits with the same underlying Chebyshev terms, but
 *                      different right hand sides
 * @tparam ORDERS...    how many Chebyshev orders to use for each of the input
 *                      variables to build the multidimensional approximation
 *                      of the output variables
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-03-03
 */
template <class T, bool USEKAHANSUM, std::size_t NIN, std::size_t NOUT, std::size_t... ORDERS>
class ChebyshevFitter {
private:
    constexpr static inline std::size_t s_ncoeffs = (ORDERS * ...);
    std::size_t m_ntotal = 0;
    std::size_t m_naccept = 0;
    std::string m_name;
    std::array<std::size_t, NIN> m_reflectIn;
    std::array<std::size_t, NOUT> m_reflectOut;
    std::array<minmax_t<T>, NIN> m_rangesIn;
    std::array<minmax_t<T>, NOUT> m_rangesOut;

    MultiDimChebyTermsTransformedCalculator<T, ORDERS...> m_termsCalc;
    std::array<LinModelFitter<T, USEKAHANSUM>, NOUT> m_fitters;

    std::vector<TH1D> m_h_in;
    std::vector<TH1D> m_h_out;
    std::vector<TH1D> m_h_outresid;
    std::vector<TH2D> m_h_in2D;
    std::vector<TH2D> m_h_out2D;
    std::vector<TH2D> m_h_outresid2D;

    std::array<std::vector<T>, NOUT> m_coeffs;

    template <std::size_t... IDX1s, std::size_t... IDX2s>
    ChebyshevFitter(std::index_sequence<IDX1s...> /* unused */,
                    std::index_sequence<IDX2s...> /* unused */,
                    const std::string_view& name,
                    const std::array<std::size_t, NIN>& reflectIn,
                    const std::array<minmax_t<T>, NIN>& rangesIn,
                    const std::array<std::size_t, NOUT>& reflectOut,
                    const std::array<minmax_t<T>, NOUT>& rangesOut,
                    const std::array<minmax_t<T>, NOUT>& rangesOutResid)
            : m_name{name}, m_reflectIn{reflectIn}, m_reflectOut{reflectOut},
              m_rangesIn{rangesIn}, m_rangesOut{rangesOut},
              m_termsCalc{rangesIn[IDX1s]...},
              m_fitters{{LinModelFitter<T, USEKAHANSUM>{
                      (static_cast<void>(IDX2s), s_ncoeffs)}...}}
    {
        if (name.empty()) throw std::runtime_error("ChebyshevFitter: empty name");
        // allocate storage for coefficients and histograms
        for (std::size_t i = 0; NOUT != i; ++i)
            m_coeffs[i].reserve(s_ncoeffs);
        m_h_in.reserve(NIN);
        m_h_in2D.reserve((NIN * (NIN - 1)) / 2);
        m_h_out.reserve(NOUT);
        m_h_out2D.reserve((NOUT * (NOUT - 1)) / 2);
        m_h_outresid.reserve(NOUT);
        m_h_outresid2D.reserve((NOUT * (NOUT - 1)) / 2);

        /// create histograms
        for (std::size_t i = 0; NIN != i; ++i) {
            m_h_in.emplace_back(
                    sfmt("h_%s_in_%02zu", m_name.c_str(), i).c_str(),
                    sfmt("x_{%zu};x_{%zu};entries", i, i).c_str(), 100,
                    (reflectIn[i] < NIN)
                            ? -std::max(std::abs(rangesIn[i].min),
                                        std::abs(rangesIn[i].max))
                            : rangesIn[i].min,
                    (reflectIn[i] < NIN) ? std::max(std::abs(rangesIn[i].min),
                                                    std::abs(rangesIn[i].max))
                                         : rangesIn[i].max);
            for (std::size_t j = 0; i != j; ++j) {
                m_h_in2D.emplace_back(
                        sfmt("h_%s_in2D_%02zu_%02zu", m_name.c_str(), i, j).c_str(),
                        sfmt("x_{%zu} vs x_{%zu};x_{%zu};x_{%zu};entries", i, j,
                             i, j)
                                .c_str(),
                        100,
                        (reflectIn[i] < NIN)
                                ? -std::max(std::abs(rangesIn[i].min),
                                            std::abs(rangesIn[i].max))
                                : rangesIn[i].min,
                        (reflectIn[i] < NIN)
                                ? std::max(std::abs(rangesIn[i].min),
                                           std::abs(rangesIn[i].max))
                                : rangesIn[i].max,
                        100,
                        (reflectIn[j] < NIN)
                                ? -std::max(std::abs(rangesIn[j].min),
                                            std::abs(rangesIn[j].max))
                                : rangesIn[j].min,
                        (reflectIn[j] < NIN)
                                ? std::max(std::abs(rangesIn[j].min),
                                           std::abs(rangesIn[j].max))
                                : rangesIn[j].max);
            }
        }
        for (std::size_t i = 0; NOUT != i; ++i) {
            m_h_out.emplace_back(
                    sfmt("h_%s_out_%02zu", m_name.c_str(), i).c_str(),
                    sfmt("y_{%zu};y_{%zu};entries", i, i).c_str(), 100,
                    (reflectOut[i] < NIN)
                            ? -std::max(std::abs(rangesOut[i].min),
                                        std::abs(rangesOut[i].max))
                            : rangesOut[i].min,
                    (reflectOut[i] < NIN)
                            ? std::max(std::abs(rangesOut[i].min),
                                       std::abs(rangesOut[i].max))
                            : rangesOut[i].max);
            m_h_outresid.emplace_back(
                    sfmt("h_%s_outresid_%02zu", m_name.c_str(), i).c_str(),
                    sfmt("y_{%zu}^{pred}-y_{%zu};y_{%zu}^{pred}-y_{%zu};"
                         "entries",
                         i, i, i, i)
                            .c_str(),
                    100, rangesOutResid[i].min, rangesOutResid[i].max);
            for (std::size_t j = 0; i != j; ++j) {
                m_h_out2D.emplace_back(
                        sfmt("h_%s_out2D_%02zu_%02zu", m_name.c_str(), i, j).c_str(),
                        sfmt("y_{%zu} vs y_{%zu};y_{%zu};y_{%zu};entries", i, j,
                             i, j)
                                .c_str(),
                        100,
                        (reflectOut[i] < NIN)
                                ? -std::max(std::abs(rangesOut[i].min),
                                            std::abs(rangesOut[i].max))
                                : rangesOut[i].min,
                        (reflectOut[i] < NIN)
                                ? std::max(std::abs(rangesOut[i].min),
                                           std::abs(rangesOut[i].max))
                                : rangesOut[i].max,
                        100,
                        (reflectOut[j] < NIN)
                                ? -std::max(std::abs(rangesOut[j].min),
                                            std::abs(rangesOut[j].max))
                                : rangesOut[j].min,
                        (reflectOut[j] < NIN)
                                ? std::max(std::abs(rangesOut[j].min),
                                           std::abs(rangesOut[j].max))
                                : rangesOut[j].max);
                m_h_outresid2D.emplace_back(
                        sfmt("h_%s_outresid2D_%02zu_%02zu", m_name.c_str(), i, j).c_str(),
                        sfmt("(y_{%zu}^{pred}-y_{%zu}) vs (y_{%zu}^{pred}-y_{%zu});"
                             "y_{%zu}^{pred}-y_{%zu};y_{%zu}^{pred}-y_{%zu};"
                             "entries",
                             i, i, j, j, i, i, j, j)
                                .c_str(),
                        100, rangesOutResid[i].min, rangesOutResid[i].max,
                        100, rangesOutResid[j].min, rangesOutResid[j].max);
            }
        }
    }

    static void
    writeRoutine(const char* fname, const char* routinename,
                 const char* varname,
                 const std::array<std::vector<double>, NOUT>& coeffs,
                 const std::array<minmax_t<double>, NIN>& minmaxes,
                 const std::array<std::size_t, NIN>& reflect_in,
                 const std::array<std::size_t, NOUT>& reflect_out)
    {
        static_assert(NIN == sizeof...(ORDERS),
                      "NIN and number of ORDERS must match.");
        constexpr std::array<std::size_t, sizeof...(ORDERS)> idxs{ORDERS...};
        FILE* f = fopen(fname, "w");
        std::fprintf(f, "#pragma once\n");
        std::fprintf(f, "\n");
        std::fprintf(f, "#include <array>\n");
        std::fprintf(f, "#include <numeric>\n");
        std::fprintf(f, "#include <type_traits>\n");
        std::fprintf(f, "#include \"ChebyshevIterator.h\"\n");
        std::fprintf(f, "\n");
        std::fprintf(f, "template <class T, class = "
                        "std::enable_if_t<std::is_floating_point_v<T>>>\n");
        std::fprintf(f,
                     "std::array<T, %zu> %s(const std::array<T, %zu>& %s) "
                     "noexcept\n",
                     NOUT, routinename, NIN, varname);
        std::fprintf(f, "{\n");
        std::fprintf(f,
                     "    constexpr std::array<std::array<T, %zu>, %zu> "
                     "_coeffs = {{\n",
                     (ORDERS * ...), NOUT);
        for (std::size_t i = 0; NOUT != i; ++i) {
            unsigned n = 0;
            std::fprintf(f, "        {");
            for (const auto& el : coeffs[i]) {
                if (0 == (n & 1)) {
                    if (n) std::fprintf(f, "         ");
                }
                std::fprintf(f, " T(%+24.16e)%s", el,
                             (1 == (n & 1)) ? ",\n" : ",");
                ++n;
            }
            std::fprintf(f, "        },\n");
        }
        std::fprintf(f, "    }};\n");
        std::fprintf(f,
                     "    constexpr std::array<transform_t<T>, %zu> "
                     "_transforms{{\n",
                     NIN);
        for (std::size_t j = 0; NIN != j; ++j) {
            std::fprintf(f,
                         "        transform_t<T>{minmax_t<T>{T(%+24.16e), "
                         "T(%+24.16e)}},\n",
                         minmaxes[j].min, minmaxes[j].max);
        }
        std::fprintf(f, "    }};\n");
        std::fprintf(f, "\n");
        const auto write_reflect_code = [&](const char* name,
                                            const auto& reflect) {
            std::fprintf(f, "    const std::array<T, %zu> %s{{\n",
                         reflect.size(), name);
            for (std::size_t i = 0; reflect.size() != i; ++i) {
                if (reflect[i] < NIN) {
                    std::fprintf(f, "        T(1 - 2 * (%s[%zu] < T(0)))%s\n",
                                 varname, reflect[i],
                                 (reflect.size() - 1 == i) ? "}};" : ",");
                } else {
                    std::fprintf(f, "        T(1)%s\n",
                                 (reflect.size() - 1 == i) ? "}};" : ",");
                }
            }
        };
        write_reflect_code("_reflect_in", reflect_in);
        write_reflect_code("_reflect_out", reflect_out);
        std::fprintf(f,
                     "    std::array<ChebyshevIterator<T, Kind::First>, %zu> "
                     "_its{\n",
                     NIN);
        for (std::size_t i = 0; NIN != i; ++i) {
            std::fprintf(f,
                         "            ChebyshevIterator<T, "
                         "Kind::First>{_transforms[%zu](_reflect_in[%zu] * "
                         "%s[%zu])}%s\n",
                         i, i, varname, i, ((NIN - 1) == i) ? "};" : ",");
        }
        const auto print1Dterms = [f, &idxs](std::size_t n) {
            std::fprintf(f, "    const std::array<T, %zu> _t%zu", idxs[n], n);
            for (std::size_t i = 0; idxs[n] != i; ++i) {
                if (!i) {
                    std::fprintf(f, "{ T(1)");
                } else {
                    std::fprintf(f, ", *++_its[%zu]", n);
                }
            }
            std::fprintf(f, "};\n");
        };
        for (std::size_t i = 0; NIN != i; ++i) print1Dterms(i);
        std::fprintf(f, "    const std::array<T, %zu> _t{\n", (ORDERS * ...));
        auto range = multiindex_range<ORDERS...>();
        for (const auto& idx : range) {
            std::fprintf(f, "        ");
            bool firstTermPrinted = false;
            for (unsigned i = 0; NIN != i; ++i) {
                if (!i) {
                    if (!idx[0] && !(idx[1] + idx[2] + idx[3] + idx[4])) {
                        std::fprintf(f, "T(1)");
                        firstTermPrinted = true;
                    } else {
                        std::fprintf(f, "_t0[%zu]", idx[0]);
                        firstTermPrinted = true;
                    }
                } else {
                    if (idx[i]) {
                        std::fprintf(f, "%s_t%u[%zu]",
                                     firstTermPrinted ? " * " : "", i,
                                     idx[i]);
                        firstTermPrinted = true;
                    }
                }
            }
            std::fprintf(f, ",\n");
        }
        std::fprintf(f, "    };\n");
        std::fprintf(f, "    return {\n");
        for (std::size_t i = 0; NOUT != i; ++i) {
            std::fprintf(f,
                         "        _reflect_out[%zu] * "
                         "std::transform_reduce(_coeffs[%zu].cbegin(), "
                         "_coeffs[%zu].cend(), _t.cbegin(), T(0))%s\n",
                         i, i, i, (NOUT - 1) == i ? "};" : ",");
        }
        std::fprintf(f, "}\n");
        fclose(f);
    }

public:
    /** @brief construct a fitter
     *
     * @param name              name of fitter
     * @param reflectIn         reflection for input coordinates
     * @param rangesIn          array of valid ranges of input coordinates
     * @param reflectOut        reflection for output coordinates
     * @param rangesOut         array of valid ranges for model outputs
     * @param rangesOutResid    array of ranges for plots of output residuals
     *
     * The name parameter is used to identify the fitter in console output,
     * ROOT files, and plots written to disk.
     *
     * The reflection parameters reflectIn and reflectOut are used to exploit
     * potential symmetries in the function to be fitted. This works by
     * specifying which coordinates must be reflected (multiplied by -1) prior
     * to the fit depending on the input. Suppose you have NIN = 3 inputs, x0,
     * x1, x2, and two outputs, y0 and y1. Further suppose that the symmetry
     * of your problem is such that if your solution for (x0, x1, x2) is (y0,
     * y1), (-x0, x1, -x2) yields (-y0, y1). To indicate this to the fitter,
     * specify in each reflect parameter depending on which input coordinate
     * you need to reflect; numbers of NIN or larger mean no reflection. In
     * the case from above where you'd reflect input coordinates 0 and 2, and
     * output coordinate 0 depending on the sign of input coordinate 0, the
     * reflectIn parameter would be {0, 3, 0} (the 3 indicating that
     * coordinate 1 does not need reflection, and coordinates 0 and 2 are
     * reflected if coordinate 0 is negative), and the reflectOut parameter
     * would be {0, 3} (with the same reasoning).
     *
     * The rangesIn parameter gives the valid input ranges (after reflection),
     * and rangesOut does likewise for the valid range of outputs (also after
     * reflection). When fitting, a pair (x, y) is rejected if their
     * coordinates fall outside the valid range; the same happens when
     * plotting when the input is outside the valid range.
     *
     * Plots of input, output and output residual (correlations) are done
     * without reflection applied.
     *
     * The rangesOutResid parameter is only used in plotting, and specifies
     * the axis ranges for the residual plots.
     */
    ChebyshevFitter(const std::string_view& name,
                    const std::array<std::size_t, NIN>& reflectIn,
                    const std::array<minmax_t<T>, NIN>& rangesIn,
                    const std::array<std::size_t, NOUT>& reflectOut,
                    const std::array<minmax_t<T>, NOUT>& rangesOut,
                    const std::array<minmax_t<T>, NOUT>& rangesOutResid)
            : ChebyshevFitter(std::make_index_sequence<NIN>(),
                              std::make_index_sequence<NOUT>(), name,
                              reflectIn, rangesIn, reflectOut, rangesOut,
                              rangesOutResid)
    {}

    void addMeas(std::array<T, NIN> x, std::array<T, NOUT> y,
                 std::array<T, NOUT> sigma_y) noexcept
    {
        ++m_ntotal;
        // work out if to reflect x (input)
        std::array<int, NIN> reflectIn;
        for (std::size_t i = 0; NIN != i; ++i)
            reflectIn[i] =
                    1 - 2 * (m_reflectIn[i] < NIN) *
                                (x[std::min(m_reflectIn[i], NIN - 1)] < 0);

        // work out if input x is in range, stop here if not
        for (std::size_t i = 0; NIN != i; ++i) {
            const auto xx = reflectIn[i] * x[i];
            if (xx < m_rangesIn[i].min || m_rangesIn[i].max < xx) return;
        }

        // work out if to reflect y (output)
        std::array<int, NOUT> reflectOut;
        for (std::size_t i = 0; NOUT != i; ++i)
            reflectOut[i] =
                    1 - 2 * (m_reflectOut[i] < NIN) *
                                (x[std::min(m_reflectOut[i], NIN - 1)] < 0);

        // work out if output y is in range, stop here if not
        for (std::size_t i = 0; NOUT != i; ++i) {
            const auto yy = reflectOut[i] * y[i];
            if (yy < m_rangesOut[i].min || m_rangesOut[i].max < yy) return;
        }

        ++m_naccept;

        // reflect
        for (std::size_t i = 0; NIN != i; ++i) x[i] *= reflectIn[i];
        for (std::size_t i = 0; NOUT != i; ++i) y[i] *= reflectOut[i];

        // calculate Chebyshev terms
        const auto terms = m_termsCalc(x);
        // add measurement y to fit (component by component)
        for (std::size_t i = 0; NOUT != i; ++i)
            m_fitters[i].addMeas(terms, y[i], sigma_y[i]);
    }

    /// returns false if any of the fits failed
    bool fit()
    {
        bool retVal = true;
        std::printf("[INFO] %s: Accepted %zu/%zu measurements (%5.1f%%)\n",
                    m_name.c_str(), m_naccept, m_ntotal, 100. * m_naccept / m_ntotal);
        for (std::size_t i = 0; NOUT != i; ++i) {
            m_coeffs[i].clear();
            auto result = m_fitters[i].fit();
            std::printf("[INFO] %s: Fitting coordinate %zu: %zu measurements, fit %s\n",
                    m_name.c_str(), i, m_fitters[i].nmeas(), result ? "OK" : "FAILED");
            retVal &= bool(result);
            if (result) {
                const auto& params = result.params();
                m_coeffs[i].assign(params.begin(), params.end());
            }
        }
        return retVal;
    }

    /** @brief do plots
     *
     * once the fit has been done, one can present the input data again to
     * make plots of residuals, inputs and output
     */
    void doPlots(std::array<T, NIN> x, std::array<T, NOUT> y,
                 std::array<T, NOUT> sigma_y) noexcept
    {
        // work out if to reflect x (input)
        std::array<int, NIN> reflectIn;
        for (std::size_t i = 0; NIN != i; ++i)
            reflectIn[i] =
                    1 - 2 * (m_reflectIn[i] < NIN) *
                                (x[std::min(m_reflectIn[i], NIN - 1)] < 0);

        // work out if input x is in range, stop here if not
        for (std::size_t i = 0; NIN != i; ++i) {
            const auto xx = reflectIn[i] * x[i];
            if (xx < m_rangesIn[i].min || m_rangesIn[i].max < xx) return;
        }

        // work out if to reflect y (output)
        std::array<int, NOUT> reflectOut;
        for (std::size_t i = 0; NOUT != i; ++i)
            reflectOut[i] =
                    1 - 2 * (m_reflectOut[i] < NIN) *
                                (x[std::min(m_reflectOut[i], NIN - 1)] < 0);

        // plot input and output (unreflected)
        for (std::size_t i = 0, k = 0; NIN != i; ++i) {
            m_h_in[i].Fill(x[i]);
            for (std::size_t j = 0; i != j; ++j, ++k)
                m_h_in2D[k].Fill(x[i], x[j]);
        }

        for (std::size_t i = 0, k = 0; NOUT != i; ++i) {
            m_h_out[i].Fill(y[i]);
            for (std::size_t j = 0; i != j; ++j, ++k)
                m_h_out2D[k].Fill(y[i], y[j]);
        }

        // reflect
        for (std::size_t i = 0; NIN != i; ++i) x[i] *= reflectIn[i];
        for (std::size_t i = 0; NOUT != i; ++i) y[i] *= reflectOut[i];

        // calculate Chebyshev terms
        const auto terms = m_termsCalc(x);

        std::array<T, NOUT> ypred;
        for (std::size_t i = 0; NOUT != i; ++i)
            ypred[i] = std::transform_reduce(terms.begin(), terms.end(),
                                             m_coeffs[i].begin(), T(0));

        // reflect back
        for (std::size_t i = 0; NIN != i; ++i) x[i] *= reflectIn[i];
        for (std::size_t i = 0; NOUT != i; ++i) y[i] *= reflectOut[i];
        for (std::size_t i = 0; NOUT != i; ++i) ypred[i] *= reflectOut[i];
        
        // plot residuals
        for (std::size_t i = 0, k = 0; NOUT != i; ++i) {
            m_h_outresid[i].Fill(ypred[i] - y[i]);
            for (std::size_t j = 0; i != j; ++j, ++k)
                m_h_outresid2D[k].Fill(ypred[i] - y[i], ypred[j] - y[j]);
        }
    }

    /** @brief destroys a ChebyshevFitter
     *
     * As a side effect, it creates a directory based on the name supplied by
     * the constructor, and places the plots and C++ code that implements the
     * approximation into it.
     *
     * It also prints some statistics to stdout.
     */
    ~ChebyshevFitter()
    {
        for (std::size_t i = 0; NIN != i; ++i) {
            std::printf("[INFO] %s: stats x[%zu]: mean %e RMS %e\n",
                    m_name.c_str(), i, m_h_in[i].GetMean(), m_h_in[i].GetRMS());
        }
        for (std::size_t i = 0; NOUT != i; ++i) {
            std::printf("[INFO] %s: stats y[%zu]: mean %e RMS %e\n",
                    m_name.c_str(), i, m_h_out[i].GetMean(), m_h_out[i].GetRMS());
        }
        for (std::size_t i = 0; NOUT != i; ++i) {
            std::printf("[INFO] %s: residual stats y_pred[%zu]-y[%zu]: mean %e RMS %e\n",
                    m_name.c_str(), i, i, m_h_outresid[i].GetMean(), m_h_outresid[i].GetRMS());
        }

        mkdir(m_name.c_str(), 0755);
        TCanvas c("c", "c", 800, 600);
        for (std::size_t i = 0; NIN != i; ++i) {
            m_h_in[i].Draw();
            c.SaveAs(sfmt("%s/x%02zu.pdf", m_name.c_str(), i).c_str());
            for (std::size_t j = 0; i != j; ++j) {
                m_h_in2D[(i * (i - 1)) / 2 + j].Draw();
                c.SaveAs(sfmt("%s/x%02zu_x%02zu.pdf", m_name.c_str(), i, j)
                                 .c_str());
            }
        }
        for (std::size_t i = 0; NOUT != i; ++i) {
            m_h_out[i].Draw();
            c.SaveAs(sfmt("%s/y%02zu.pdf", m_name.c_str(), i).c_str());
            m_h_outresid[i].Draw();
            c.SaveAs(sfmt("%s/yresid%02zu.pdf", m_name.c_str(), i).c_str());
            for (std::size_t j = 0; i != j; ++j) {
                m_h_out2D[(i * (i - 1)) / 2 + j].Draw();
                c.SaveAs(sfmt("%s/y%02zu_y%02zu.pdf", m_name.c_str(), i, j)
                                 .c_str());
                m_h_outresid2D[(i * (i - 1)) / 2 + j].Draw();
                c.SaveAs(sfmt("%s/yresid%02zu_yresid%02zu.pdf",
                              m_name.c_str(), i, j)
                                 .c_str());
            }
        }

        writeRoutine(sfmt("%s/%s.h", m_name.c_str(), m_name.c_str()).c_str(),
                     m_name.c_str(), "x", m_coeffs, m_rangesIn, m_reflectIn,
                     m_reflectOut);
    }
};

// vim: tw=78:sw=4:ft=cpp:et
