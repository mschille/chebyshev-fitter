/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file MultiIndex.h
 *
 * @brief simple MultiIndex class
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-17
 */
#pragma once

#include <array>
#include <tuple>
#include <type_traits>
#include <utility>

namespace impl { ///< implementation details
    template <class T>
    using remove_cvref_t = std::remove_cv_t<std::remove_reference_t<T>>;
    template <class T, class U>
    using enable_if_compatible_t =
            std::enable_if_t<std::is_same_v<U, remove_cvref_t<T>>>;
} // namespace impl

/** @brief MultiIndex class
 *
 * This class models a multi-index (i_1, i_2, i_3, ...) with up to RANK
 * components.
 *
 * @tparam RANK         number of indices in the multi-index
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-17
 */
template <std::size_t RANK>
class MultiIndex final {
public:
    /// number of indices in this multi-index
    constexpr static inline std::size_t rank = RANK;
    /// type used for the state of the multi-index
    using index_type = std::array<std::size_t, RANK>;

private:
    /// state of the multi-index
    index_type m_state;
    /// const reference to the number of valid indices in each component
    const index_type& m_last;

    /// helper class (empty)
    struct tag_last {};
    /// construct the MultiIndex that corresponds to the end()
    template <class T, class = impl::enable_if_compatible_t<index_type, T>>
    constexpr MultiIndex(tag_last /* unused */, T&& dimensions) noexcept
            : m_state{std::forward<T>(dimensions)}, m_last{m_state}
    {}

    /// helper class (empty)
    struct tag_first {};
    /// construct the MultiIndex that corresponds to the begin()
    constexpr MultiIndex(tag_first /* unused */,
                         const MultiIndex& other) noexcept
            : m_state{}, m_last{other.m_last}
    {}

public:
    /// type to encapsulate a range of multi-indices
    class multiindex_range final {
    private:
        /// keep an instance of end() around
        MultiIndex m_last;

    public:
        /// constructor (takes a lastindex for each component)
        template <class T,
                  class = impl::enable_if_compatible_t<index_type, T>>
        constexpr explicit multiindex_range(T&& lastindex) noexcept
                : m_last{tag_last{}, std::forward<T>(lastindex)}
        {}

        /// return the end of the range
        [[nodiscard]] constexpr const MultiIndex& end() const noexcept
        {
            return m_last;
        }
        /// return the begin of the range
        [[nodiscard]] constexpr MultiIndex begin() const noexcept
        {
            return {tag_first{}, end()};
        }
    };

    /// prefix-increment MultiIndex (returns value after incrementing)
    constexpr MultiIndex& operator++() noexcept
    {
        for (std::size_t i = 0;;) {
            if (++m_state[i] != m_last[i]) break;
            if (rank != ++i) {
                for (std::size_t k = 0; i != k; ++k) m_state[k] = 0;
                continue;
            }
            m_state = m_last;
            break;
        }
        return *this;
    }

    /// postfix-increment of MultiIndex (returns value before incrementing)
    [[nodiscard]] constexpr MultiIndex operator++(int /* unused */) noexcept
    {
        auto retVal{*this};
        operator++();
        return retVal;
    }

    /// return current state of the MultiIndex
    constexpr const auto& operator*() const noexcept { return m_state; }
    constexpr std::size_t operator[](std::size_t idx) const noexcept
    {
        return m_state[idx];
    }

    /// compare two MultiIndex instances for equality
    [[nodiscard]] friend constexpr bool
    operator==(const MultiIndex& it, const MultiIndex& jt) noexcept
    {
        return it.m_state == jt.m_state;
    }
    /// compare two MultiIndex instances for inequality
    [[nodiscard]] friend constexpr bool
    operator!=(const MultiIndex& it, const MultiIndex& jt) noexcept
    {
        return it.m_state != jt.m_state;
    }
};

/// return a range of multi-indices from (0, ...) to (lastindex, ...)
template <class T,
          std::size_t RANK = std::tuple_size_v<impl::remove_cvref_t<T>>,
          class = impl::enable_if_compatible_t<std::array<std::size_t, RANK>,
                                               T>>
[[nodiscard]] constexpr typename MultiIndex<RANK>::multiindex_range
multiindex_range(T&& lastindex) noexcept
{
    return typename MultiIndex<RANK>::multiindex_range{
            std::forward<T>(lastindex)};
}

namespace impl {
    struct to_indices {
        template <std::size_t... IDXs, class T>
        constexpr auto operator()(std::index_sequence<IDXs...> /* unused */,
                                  const T& lastindex) const noexcept
        {
            return std::array<std::size_t, sizeof...(IDXs)>{
                    static_cast<std::size_t>(lastindex[IDXs])...};
        }
    };
} // namespace impl

/// return a range of multi-indices from (0, ...) to (lastindex, ...)
template <class T,
          std::size_t RANK = std::tuple_size_v<impl::remove_cvref_t<T>>,
          class ELEMTYPE = std::tuple_element_t<0, impl::remove_cvref_t<T>>,
          class = std::enable_if_t<!std::is_same_v<std::size_t, ELEMTYPE> &&
                                   std::is_integral_v<ELEMTYPE>>>
[[nodiscard]] constexpr auto multiindex_range(T&& lastindex) noexcept
{
    return multiindex_range(impl::to_indices{}(
            std::make_index_sequence<RANK>{}, std::forward<T>(lastindex)));
}

/// return a range of multi-indices from (0, ...) to (LASTINDICES, ...)
template <std::size_t... LASTINDICES>
[[nodiscard]] constexpr auto multiindex_range() noexcept
{
    return multiindex_range(
            std::array<std::size_t, sizeof...(LASTINDICES)>{LASTINDICES...});
}

// vim: sw=4:tw=78:ft=cpp:et
