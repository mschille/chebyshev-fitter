/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file MultiDimChebyTermsCalculator.h
 *
 * @brief calculate terms in a multi-dimensional Chebyshev up to a given order
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-19
 */
#pragma once

#include <array>
#include <cassert>
#include <iterator>
#include <type_traits>
#include <utility>
#include <span>

#include "ChebyshevIterator.h"
#include "MultiIndex.h"

/** @brief calculate terms of Chebyshev polynomials (1st kind) up to MAXORDERS
 *
 * This class calculates a vector filled with products T_i(x) * T_j(y) * ...,
 * where the indices run from 0 to the numbers given by MAXORDERS. The output
 * is ordered the way in which the MultiIndex class iterates over the
 * multiindices.
 *
 * There is a operator() method which triggers calculating the output based on
 * the given coordinates (each of which is expexted to be in the interval
 * [-1, +1], as is conventional when working with Chebyshev polynomials). It
 * returns a const reference to the internal storage in which the coefficients
 * are calculated. The calculated results are valid until the next call to
 * operator().
 *
 * @tparam T            floating point type used for calculations
 * @tparam MAXORDERS    max. order to which to calculate terms per dimension
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date   2024-02-19
 */
template <class T, std::size_t... MAXORDERS>
class MultiDimChebyTermsCalculator {
public:
    /// get number of coefficients
    constexpr static std::size_t size() noexcept { return (MAXORDERS * ...); }
    /// get rank (number of indices)
    constexpr static std::size_t rank() noexcept
    {
        return sizeof...(MAXORDERS);
    }

    using point_t = std::array<T, rank()>;
    using value_type = std::span<const T>;

private:
    using index_t = std::array<std::size_t, rank()>;
    using storage_t = std::array<T, size()>;

    /// storage to save terms in
    storage_t m_terms;

    /// helper for build1DTerms below
    template <std::size_t... IDXs>
    [[nodiscard]] constexpr static std::array<T, 1 + sizeof...(IDXs)>
    _build1DTerms(std::index_sequence<IDXs...> /* unused */,
                  const float& x) noexcept
    {
        ChebyshevIterator<T, Kind::First> it{x};
        return {T(1), (static_cast<void>(IDXs), *++it)...};
    }

    /// type used for Chebyshev terms in 1D
    using terms1d_t = std::tuple<std::array<T, MAXORDERS>...>;

    /// build tuple of 1D arrays { T_0(p[i]), T_1(p[i]), ... } for each i in p
    template <std::size_t... IDXs>
    [[nodiscard]] constexpr static terms1d_t
    build1DTerms(std::index_sequence<IDXs...> /* unused */,
                 const point_t& p) noexcept
    {
        return {_build1DTerms(std::make_index_sequence<MAXORDERS - 1>{},
                              p[IDXs])...};
    }

    /// return product of 1D terms for given multi-index idx
    template <std::size_t... IDXs>
    constexpr static T buildNDTerm(std::index_sequence<IDXs...> /* unused */,
                                   const terms1d_t& terms1d,
                                   const index_t& idx) noexcept
    {
        return ((std::get<IDXs>(terms1d)[idx[IDXs]]) * ...);
    }

    template <std::size_t... IDXs>
    [[nodiscard]] constexpr static storage_t
    buildNDTerms(std::index_sequence<IDXs...> /* unused */,
                 const terms1d_t& terms1d) noexcept
    {
        const auto range = multiindex_range(index_t{MAXORDERS...});
        auto it = range.begin();
        return {{(static_cast<void>(IDXs),
                  buildNDTerm(std::make_index_sequence<rank()>{}, terms1d,
                              *it++))...}};
    }

public:
    /// set point, and calculate terms
    value_type operator()(const point_t& p) noexcept
    {
        m_terms = buildNDTerms(
                std::make_index_sequence<size()>{},
                build1DTerms(std::make_index_sequence<rank()>{}, p));
        return {m_terms.data(), size()};
    }
};

// vim: tw=78:sw=4:ft=cpp:et
