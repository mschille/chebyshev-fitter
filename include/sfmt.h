/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file sfmt.h
 *
 * @brief safe C++ string formatting based on printf-style format strings
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-29
 */
#pragma once

#include <cerrno>
#include <cstdio>
#include <string>
#include <system_error>
#include <type_traits>

/** @brief format using printf-style format-string
 *
 * @tparam ARGS...      types of argument to be formatted
 *
 * @param fmtstr        format string (printf-style)
 * @param args          arguments to be formatted
 *
 * @returns std::string with formatted string
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-29
 */
template <class... ARGS,
          class = std::enable_if_t<((std::is_scalar_v<ARGS>) && ...)>>
std::string sfmt(const char* fmtstr, ARGS... args)
{
    int sz = std::snprintf(nullptr, 0, fmtstr, args...);
    if (sz >= 0) {
        std::string retVal;
        retVal.reserve(std::size_t(sz) + 1);
        retVal.resize(std::size_t(sz));
        if (std::snprintf(retVal.data(), std::size_t(sz) + 1, fmtstr,
                          args...) >= 0)
            return retVal;
    }
    throw std::system_error(errno, std::generic_category(), __func__);
}

// vim: tw=78:sw=4:ft=cpp:et
