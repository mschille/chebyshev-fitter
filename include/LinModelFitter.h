/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file LinModelFitter.h
 *
 * @brief χ² fit to a model that is linear in fit parameters
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-19
 */
#pragma once

#include <algorithm>
#include <cassert>
#include <optional>
#include <span>
#include <vector>

#include "Math/CholeskyDecomp.h"
#include "KahanSum.h"

template <class T>
class LinModelFitterBase {
public:
    class FitResult {
    private:
        ROOT::Math::CholeskyDecompGenDim<T> m_decomp;
        std::vector<T> m_storage;
        std::span<T> m_cov;
        std::span<T> m_rhs;
        bool m_solved = false;
        bool m_inverted = false;

    public:
        /// constructor from a matrix and a rhs vector
        FitResult(std::span<const T> mat, std::span<const T> rhs)
                : m_decomp(rhs.size(), mat.data()),
                  m_storage(mat.size() + rhs.size()),
                  m_cov{m_storage.data(), mat.size()},
                  m_rhs{m_storage.data() + mat.size(), rhs.size()}
        {
            if (m_decomp) std::copy(rhs.begin(), rhs.end(), m_rhs.begin());
        }

        /// return true if the fit result is valid
        operator bool() const noexcept { return bool(m_decomp); }
        /// return true is the fit result is invalid
        bool operator!() const noexcept { return !m_decomp; }

        /// return fit parameters
        const std::span<const T> params() noexcept
        {
            if (m_decomp) {
                m_decomp.Solve(m_rhs);
                m_solved = true;
            }
            return {m_rhs.data(), m_rhs.size()};
        }

        /// return (packed) covariance matrix
        const std::span<const T> packedcov()
        {
            if (m_decomp) {
                m_decomp.Invert(m_cov.data());
                m_inverted = true;
            }
            return {m_cov.data(), m_cov.size()};
        }
    };
};

/** @brief χ² fitter for a model that is linear in fit parameters
 *
 * Perform a fit by minimising χ² = ∑ₖ (𝑦ₖ − 𝑚(𝑥ₖ; 𝑝ᵢ...))²/σ²ₖ, where 𝑦ₖ is
 * the measurement, σₖ is the uncertainty, 𝑥ₖ is the point at which the
 * measurement is taken, and 𝑝ᵢ are the fit parameters. The fit model must be
 * linear in the fit parameters, m = ∑ⱼ 𝑝ⱼ⋅ 𝑓ⱼ(𝑥) with 𝜕𝑓ⱼ/𝜕𝑝ᵢ = 0, or, in
 * other words, the derivative of m with respect to the 𝑝ᵢ must not depend on
 * any of the 𝑝ᵢ.
 *
 * @tparam T            floating point type to use for calculations
 * @tparam USEKAHANSUM  use Kahan summation when accumulating matrix and
 *                      right hand side(s)
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-19
 */
template <class T, bool USEKAHANSUM = false>
class LinModelFitter : public LinModelFitterBase<T> {
protected:
    /// number of parameters
    std::size_t m_nparams = 0;
    /// number of measurements
    std::size_t m_nmeas = 0;
    /// storage (only allocate once for all those Ts)
    std::vector<T> m_storage;

    /// return size of matrix in number of Ts
    std::size_t matsz() const noexcept
    {
        return (m_nparams * (m_nparams + 1)) / 2;
    }
    /// carve out span for matrix from m_storage
    std::span<T> _mat() noexcept { return {m_storage.data(), matsz()}; }
    /// carve out span for matrix carries from m_storage
    std::span<T> _mat_carry() noexcept
    {
        return {m_storage.data() + USEKAHANSUM * matsz(), matsz()};
    }
    /// carve out span for wksp2 from m_storage
    std::span<T> _wksp2() noexcept
    {
        return {m_storage.data() + (1 + USEKAHANSUM) * matsz(), matsz()};
    }
    /// carve out span for right hand side from m_storage
    std::span<T> _rhs() noexcept
    {
        return {m_storage.data() + (2 + USEKAHANSUM) * matsz(), m_nparams};
    }
    /// carve out span for right hand side carries from m_storage
    std::span<T> _rhs_carry() noexcept
    {
        return {m_storage.data() + (2 + USEKAHANSUM) * matsz() +
                        USEKAHANSUM * m_nparams,
                m_nparams};
    }
    /// carve out span for wksp from m_storage
    std::span<T> _wksp() noexcept
    {
        return {m_storage.data() + (2 + USEKAHANSUM) * matsz() +
                        (1 + USEKAHANSUM) * m_nparams,
                m_nparams};
    }

public:
    /// create a fitter for nparams parameters
    explicit LinModelFitter(std::size_t nparams)
            : m_nparams{nparams},
              m_storage((2 + USEKAHANSUM) * (nparams * (nparams + 1)) / 2 +
                                (2 + USEKAHANSUM) * nparams,
                        T(0))
    {}

    /// return number of measurements
    std::size_t nmeas() const noexcept
    { return m_nmeas; }

    /// clear all measurements
    void clear() noexcept
    {
        m_storage.assign(m_storage.size(), T(0));
        m_nmeas = 0;
    }

    /** @brief add a measurement
     *
     * @param modelderivs       gradient of model wrt. fit parameters (𝜕𝑚/𝜕𝑝ᵢ)
     * @param y                 measurement 𝑦
     * @param sigma_y           uncertainty σ on y
     */
    void addMeas(std::span<const T> modelderivs, T y, T sigma_y = 1) noexcept
    {
        auto mat = _mat();
        auto wksp2 = _wksp2();
        auto rhs = _rhs();
        auto wksp = _wksp();
        const auto sz = rhs.size();
        const auto matsz = mat.size();
        assert(sz == modelderivs.size());
        ++m_nmeas;
        {
            const T w = T(1) / sigma_y;
            y *= w;
            for (std::size_t i = 0; sz != i; ++i)
                wksp[i] = w * modelderivs[i];
        }
        // build update to matrix - this is slow because of the funky memory
        // access pattern of indices i and j. We do therefore not add this
        // straight into m_mat, but to a temporary workspace (so we have the
        // potential to exploit SIMD parallelism in the matrix update)
        for (std::size_t i = 0, k = 0; sz != i; ++i) {
            const T wkspi = wksp[i];
            for (std::size_t j = 0; j <= i; ++j, ++k)
                wksp2[k] = wkspi * wksp[j];
        }
        // update matrix and right hand side
        if constexpr (!USEKAHANSUM) {
            for (std::size_t i = 0; sz != i; ++i)
                rhs[i] += y * wksp[i];
            for (std::size_t k = 0; matsz != k; ++k)
                mat[k] += wksp2[k];
        } else {
            auto rhs_carry = _rhs_carry();
            for (std::size_t i = 0; sz != i; ++i)
                KahanSum<T>{rhs[i], rhs_carry[i]} += y * wksp[i];
            auto mat_carry = _mat_carry();
            for (std::size_t k = 0; matsz != k; ++k)
                KahanSum<T>{mat[k], mat_carry[k]} += wksp2[k];
        }
    }

    using typename LinModelFitterBase<T>::FitResult;

    /// fit, return a fit result
    FitResult fit()
    {
        auto mat = _mat();
        auto rhs = _rhs();
        return {{mat.data(), mat.size()}, {rhs.data(), rhs.size()}};
    }
};

// vim: tw=78:sw=4:ft=cpp:et
