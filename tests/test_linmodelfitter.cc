/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file test_chebysheviterator.cc
 *
 * @brief unit test for the LinModelFitter class
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-19
 */
#include <cstdio>
#include <cmath>
#include <limits>

#include "LinModelFitter.h"
#include "Test.h"

/// simple unit test for the LinModelFitter class
static bool test_linmodelfitter(void)
{
    Test t(__func__);
    constexpr float x[] = { -1.f, 0.f, 1.f };
    constexpr float y[] = { 1.f, 0.f, 1.f };
    constexpr float sigma_y[] = { 1.f, 1.f, 1.f };
    constexpr float ref_par[] = { 0.f, 0.f, 1.f };
    constexpr float ref_cov[] = { 1.f, 0.f, .5f, -1.f, 0.f, 1.5f };
    using std::abs;
    using std::numeric_limits;
    LinModelFitter<float> fitter(3);
    std::vector<float> modelderiv(3, 0.f);

    for (std::size_t i = 0; 3 != i; ++i) {
        if (i != fitter.nmeas()) fail(t);
        modelderiv[0] = 1.f;
        modelderiv[1] = x[i];
        modelderiv[2] = x[i] * x[i];
        fitter.addMeas(modelderiv, y[i], sigma_y[i]);
    }
    if (3 != fitter.nmeas()) fail(t);

    auto result = fitter.fit();
    if (!result) fail(t);
    const auto& params = result.params();
    if (3 != params.size()) fail(t);
    constexpr auto eps = numeric_limits<float>::epsilon();
    if (abs(params[0] - ref_par[0]) > abs(3.f * eps * ref_par[0])) fail(t);
    if (abs(params[1] - ref_par[1]) > abs(3.f * eps * ref_par[1])) fail(t);
    if (abs(params[2] - ref_par[2]) > abs(3.f * eps * ref_par[2])) fail(t);
    const auto cov = result.packedcov();
    if (6 != cov.size()) fail(t);
    if (abs(cov[0] - ref_cov[0]) > abs(3.f * eps * ref_cov[0])) fail(t);
    if (abs(cov[1] - ref_cov[1]) > abs(3.f * eps * ref_cov[1])) fail(t);
    if (abs(cov[2] - ref_cov[2]) > abs(3.f * eps * ref_cov[2])) fail(t);
    if (abs(cov[3] - ref_cov[3]) > abs(3.f * eps * ref_cov[3])) fail(t);
    if (abs(cov[4] - ref_cov[4]) > abs(3.f * eps * ref_cov[4])) fail(t);
    if (abs(cov[5] - ref_cov[5]) > abs(3.f * eps * ref_cov[5])) fail(t);

    pass(t);
}

int main(int /* unused */, char* argv[])
{
    return run_test_suite(argv[0], {test_linmodelfitter});
}

// vim: tw=78:sw=4:ft=cpp:et
