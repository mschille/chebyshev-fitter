/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file test_sfmt.cc
 *
 * @brief unit test for sfmt routine
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-29
 */
#include <cstdio>
#include <cmath>
#include <limits>

#include "sfmt.h"
#include "Test.h"

/// simple unit test for sfmt
static bool test_sfmt(void)
{
    Test t(__func__);
    std::string str = sfmt("Foo % 3d", 42);
    if ("Foo  42" != str) fail(t);
    pass(t);
}

int main(int /* unused */, char* argv[])
{
    return run_test_suite(argv[0], {test_sfmt});
}

// vim: tw=78:sw=4:ft=cpp:et
