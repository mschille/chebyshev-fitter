/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file test_multidimchebytermscalculator.cc
 *
 * @brief unit test for the MultiDimChebyTermsCalculator class
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-19
 */
#include <cstdio>

#include "MultiDimChebyTermsCalculator.h"
#include "Test.h"

/// simple unit test for MultiDimChebyTermsCalculator
static bool test_multidimchebytermscalculator(void)
{
    Test t(__func__);
    constexpr float refT_at_zero[] = {1.f, 0.f, -1.f};
    constexpr float refT_at_half[] = {1.f, 0.5f, -.5f};
    constexpr float refT_at_quarter[] = {1.f, 0.25f, -.875f};
    constexpr float ref_terms[] = {
            refT_at_zero[0] * refT_at_half[0] * refT_at_quarter[0],
            refT_at_zero[1] * refT_at_half[0] * refT_at_quarter[0],
            refT_at_zero[2] * refT_at_half[0] * refT_at_quarter[0],
            refT_at_zero[0] * refT_at_half[1] * refT_at_quarter[0],
            refT_at_zero[1] * refT_at_half[1] * refT_at_quarter[0],
            refT_at_zero[2] * refT_at_half[1] * refT_at_quarter[0],
            refT_at_zero[0] * refT_at_half[2] * refT_at_quarter[0],
            refT_at_zero[1] * refT_at_half[2] * refT_at_quarter[0],
            refT_at_zero[2] * refT_at_half[2] * refT_at_quarter[0],
            refT_at_zero[0] * refT_at_half[0] * refT_at_quarter[1],
            refT_at_zero[1] * refT_at_half[0] * refT_at_quarter[1],
            refT_at_zero[2] * refT_at_half[0] * refT_at_quarter[1],
            refT_at_zero[0] * refT_at_half[1] * refT_at_quarter[1],
            refT_at_zero[1] * refT_at_half[1] * refT_at_quarter[1],
            refT_at_zero[2] * refT_at_half[1] * refT_at_quarter[1],
            refT_at_zero[0] * refT_at_half[2] * refT_at_quarter[1],
            refT_at_zero[1] * refT_at_half[2] * refT_at_quarter[1],
            refT_at_zero[2] * refT_at_half[2] * refT_at_quarter[1],
            refT_at_zero[0] * refT_at_half[0] * refT_at_quarter[2],
            refT_at_zero[1] * refT_at_half[0] * refT_at_quarter[2],
            refT_at_zero[2] * refT_at_half[0] * refT_at_quarter[2],
            refT_at_zero[0] * refT_at_half[1] * refT_at_quarter[2],
            refT_at_zero[1] * refT_at_half[1] * refT_at_quarter[2],
            refT_at_zero[2] * refT_at_half[1] * refT_at_quarter[2],
            refT_at_zero[0] * refT_at_half[2] * refT_at_quarter[2],
            refT_at_zero[1] * refT_at_half[2] * refT_at_quarter[2],
            refT_at_zero[2] * refT_at_half[2] * refT_at_quarter[2]};

    MultiDimChebyTermsCalculator<float, 3, 3, 3> calc;
    const auto terms = calc({0.f, 0.5f, 0.25f});

    if (27 != terms.size()) fail(t);

    // do not write a for loop here, so we can isolate where it fails...
    if (ref_terms[ 0] != terms[ 0]) fail(t);
    if (ref_terms[ 1] != terms[ 1]) fail(t);
    if (ref_terms[ 2] != terms[ 2]) fail(t);
    if (ref_terms[ 3] != terms[ 3]) fail(t);
    if (ref_terms[ 4] != terms[ 4]) fail(t);
    if (ref_terms[ 5] != terms[ 5]) fail(t);
    if (ref_terms[ 6] != terms[ 6]) fail(t);
    if (ref_terms[ 7] != terms[ 7]) fail(t);
    if (ref_terms[ 8] != terms[ 8]) fail(t);
    if (ref_terms[ 9] != terms[ 9]) fail(t);
    if (ref_terms[10] != terms[10]) fail(t);
    if (ref_terms[11] != terms[11]) fail(t);
    if (ref_terms[12] != terms[12]) fail(t);
    if (ref_terms[13] != terms[13]) fail(t);
    if (ref_terms[14] != terms[14]) fail(t);
    if (ref_terms[15] != terms[15]) fail(t);
    if (ref_terms[16] != terms[16]) fail(t);
    if (ref_terms[17] != terms[17]) fail(t);
    if (ref_terms[18] != terms[18]) fail(t);
    if (ref_terms[19] != terms[19]) fail(t);
    if (ref_terms[20] != terms[20]) fail(t);
    if (ref_terms[21] != terms[21]) fail(t);
    if (ref_terms[22] != terms[22]) fail(t);
    if (ref_terms[23] != terms[23]) fail(t);
    if (ref_terms[24] != terms[24]) fail(t);
    if (ref_terms[25] != terms[25]) fail(t);
    if (ref_terms[26] != terms[26]) fail(t);

    pass(t);
}

int main(int /* unused */, char* argv[])
{
    return run_test_suite(argv[0], {test_multidimchebytermscalculator});
}

// vim: tw=78:sw=4:ft=cpp:et
