/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file test_chebysheviterator.cc
 *
 * @brief unit test for the ChebyshevIterator class
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-19
 */
#include <cstdio>

#include "ChebyshevIterator.h"
#include "Test.h"

/// simple unit test for the ChebyshevIterator class (1st kind)
static bool test_chebysheviteratorfirstkind(void)
{
    Test t(__func__);
    // T_n(x) = cos(n * acos(x)) for x in [-1, +1]
    const float ref_T[] = {1.f, 0.5f, -.5f, -1.f};

    ChebyshevIterator<float, Kind::First> it{0.5f};
    if (ref_T[0] != *it++) fail(t);
    if (ref_T[1] != *it++) fail(t);
    if (ref_T[2] != *it++) fail(t);
    if (ref_T[3] != *it++) fail(t);

    pass(t);
}

/// simple unit test for the ChebyshevIterator class (2nd kind)
static bool test_chebysheviteratorsecondkind(void)
{
    Test t(__func__);
    // U_n(x) = sin((n + 1) * acos(x)) / sin(acos(x)) for x in [-1, +1]
    const float ref_U[] = {1.f, 1.f, 0.f, -1.f};

    ChebyshevIterator<float, Kind::Second> it{0.5f};
    if (ref_U[0] != *it++) fail(t);
    if (ref_U[1] != *it++) fail(t);
    if (ref_U[2] != *it++) fail(t);
    if (ref_U[3] != *it++) fail(t);

    pass(t);
}

/// simple unit test for the ChebyshevIterator class (3rd kind)
static bool test_chebysheviteratorthirdkind(void)
{
    Test t(__func__);
    // V_n(x) = cos((n + 1/2) * acos(x)) / cos(acos(x) / 2) for x in [-1, +1]
    const float ref_V[] = {1.f, 0.f, -1.f, -1.f};

    ChebyshevIterator<float, Kind::Third> it{0.5f};
    if (ref_V[0] != *it++) fail(t);
    if (ref_V[1] != *it++) fail(t);
    if (ref_V[2] != *it++) fail(t);
    if (ref_V[3] != *it++) fail(t);

    pass(t);
}

/// simple unit test for the ChebyshevIterator class (4th kind)
static bool test_chebysheviteratorfourthkind(void)
{
    Test t(__func__);
    // W_n(x) = sin((n + 1/2) * acos(x)) / sin(acos(x) / 2) for x in [-1, +1]
    const float ref_W[] = {1.f, 2.f, 1.f, -1.f};

    ChebyshevIterator<float, Kind::Fourth> it{0.5f};
    if (ref_W[0] != *it++) fail(t);
    if (ref_W[1] != *it++) fail(t);
    if (ref_W[2] != *it++) fail(t);
    if (ref_W[3] != *it++) fail(t);

    pass(t);
}

int main(int /* unused */, char* argv[])
{
    return run_test_suite(argv[0], {test_chebysheviteratorfirstkind,
                                    test_chebysheviteratorsecondkind,
                                    test_chebysheviteratorthirdkind,
                                    test_chebysheviteratorfourthkind});
}

// vim: tw=78:sw=4:ft=cpp:et
