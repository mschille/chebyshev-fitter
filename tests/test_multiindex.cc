/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
/** @file test_multiindex.h
 *
 * @brief unit test for the MultiIndex class
 *
 * @author Manuel Schiller <Manuel.Schiller@glasgow.ac.uk>
 * @date 2024-02-19
 */
#include <cstdio>

#include "MultiIndex.h"
#include "Test.h"

/// simple unit test for the MultiIndex class
static bool test_multiindex(void)
{
    Test t(__func__);
    const auto range = multiindex_range<2, 1, 3>();
    auto it = range.begin();
    if (it == range.end()) fail(t);
    if (std::array<std::size_t, 3>{0, 0, 0} != *it++) fail(t);
    if (it == range.end()) fail(t);
    if (std::array<std::size_t, 3>{1, 0, 0} != *it++) fail(t);
    if (it == range.end()) fail(t);
    if (std::array<std::size_t, 3>{0, 0, 1} != *it++) fail(t);
    if (it == range.end()) fail(t);
    if (std::array<std::size_t, 3>{1, 0, 1} != *it++) fail(t);
    if (it == range.end()) fail(t);
    if (std::array<std::size_t, 3>{0, 0, 2} != *it++) fail(t);
    if (it == range.end()) fail(t);
    if (std::array<std::size_t, 3>{1, 0, 2} != *it++) fail(t);
    if (it != range.end()) fail(t);

    pass(t);
}

int main(int /* unused */, char* argv[])
{
    return run_test_suite(argv[0], {test_multiindex});
}

// vim: tw=78:sw=4:ft=cpp:et
