# Copyright (C) CERN for the benefit of the LHCb collaboration
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

TESTS.CXX=tests/test_multiindex tests/test_chebysheviterator \
      tests/test_multidimchebytermscalculator \
      tests/test_multidimchebytermstransformedcalculator \
      tests/test_linmodelfitter tests/test_kahansum tests/test_sfmt
TARGETS.CXX=$(TESTS.CXX) statepropfitter/main

CFLAGS=-std=c11 -Wall -Wextra -Wpedantic -Iinclude -O2 -ftree-vectorize -g
CXXFLAGS=-std=c++20 -Wall -Wextra -Wpedantic -Iinclude -O2 -ftree-vectorize -g

TESTS=$(TESTS.C) $(TESTS.CXX)

.PHONY: all clean distclean test run-tests

all: $(TARGETS.C) $(TARGETS.CXX)

clean:
	rm -f *.o statepropfitter/*.o testresults/*.out testresults/*.passed

distclean: clean
	rm -f $(TARGETS.C) $(TARGETS.CXX)
	rm -fr testresults

test: $(TESTS) run-tests

run-tests: testresults \
	$(patsubst tests/%,testresults/%.out,$(TESTS)) \
	$(patsubst tests/%,testresults/%.passed,$(TESTS))
	@echo ALL TESTS PASSED.

testresults:
	mkdir -p testresults

testresults/%.out testresults/%.passed: tests/%
	@$(dir $<)$(notdir $<) 2>&1 && \
		touch $(patsubst %.out,%.passed, $@) | tee $@

$(TARGETS.CXX): CC=$(CXX)
$(TARGETS.CXX): CFLAGS=$(CXXFLAGS)

tests/test_multiindex.o: tests/test_multiindex.cc include/MultiIndex.h \
	include/Test.h
tests/test_chebysheviterator.o: tests/test_chebysheviterator.cc \
	include/ChebyshevIterator.h include/Test.h
tests/test_multidimchebytermscalculator.o: \
	tests/test_multidimchebytermscalculator.cc \
	include/MultiDimChebyTermsCalculator.h include/ChebyshevIterator.h \
	include/MultiIndex.h include/Test.h
tests/test_multidimchebytermstransformedcalculator.o: \
	tests/test_multidimchebytermstransformedcalculator.cc \
	include/MultiDimChebyTermsTransformedCalculator.h \
	include/MultiDimChebyTermsCalculator.h include/ChebyshevIterator.h \
	include/MultiIndex.h include/Test.h
tests/test_linmodelfitter.o: CXXFLAGS+=-I$(shell root-config --incdir)
tests/test_linmodelfitter.o: tests/test_linmodelfitter.cc \
	include/LinModelFitter.h include/Test.h
tests/test_kahansum.o: tests/test_kahansum.cc include/KahanSum.h \
	include/Test.h
tests/test_sfmt.o: tests/test_sfmt.cc include/sfmt.h include/Test.h
statepropfitter/TupleReader2.o: CXXFLAGS+=-I$(shell root-config --incdir)
statepropfitter/TupleReader2.o: statepropfitter/TupleReader2.C \
	statepropfitter/TupleReader2.h include/ChebyshevFitter.h \
	include/ChebyshevIterator.h include/KahanSum.h \
	include/LinModelFitter.h include/MultiDimChebyTermsCalculator.h \
	include/MultiDimChebyTermsTransformedCalculator.h \
	include/MultiIndex.h include/progbar.h include/sfmt.h
statepropfitter/main.o: CXXFLAGS+=-I$(shell root-config --incdir)
statepropfitter/main.o: statepropfitter/main.cc statepropfitter/TupleReader2.h
statepropfitter/main: LDLIBS=$(shell root-config --libs)
statepropfitter/main: statepropfitter/main.o statepropfitter/TupleReader2.o

