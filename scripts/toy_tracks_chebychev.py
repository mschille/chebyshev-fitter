# Copyright (C) CERN for the benefit of the LHCb collaboration
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#####
# Authors : Roel Aaij, Renato Quagliani
# lb-run Brunel/latest python makeToyTracks.py
# lb-run Moore/v50r0  python toy_tracks.py [ should work ]
# Edit your favourite rangees to generate things
####
from __future__ import print_function
import math
import sys
import time

import numpy as np
# Phase 2: GaudiPython
from GaudiPython.Bindings import gbl, AppMgr
from ROOT import gInterpreter, Math, TLorentzVector
from math import cosh, pi
import ROOT
from Configurables import LHCbApp, CondDB
#from LHCb import ParticleID
# Get access to units
gInterpreter.Declare('#include <GaudiKernel/SystemOfUnits.h>')
Units = gbl.Gaudi.Units
import math 
def track_vector2(x, y, tx, ty, p, charge):   
    m = 105.66
    pz = p / math.sqrt( 1. + tx**2 + ty**2)
    E = math.sqrt( p**2 + m**2)
    v = TLorentzVector( tx*pz , ty*pz, pz , E)
    return gbl.Gaudi.TrackVector(x, y, tx, ty, charge / p)
app = LHCbApp(
    DataType="Upgrade",
    EvtMax=50,
    Simulation=True,
    DDDBtag   = "dddb-20231017",
    CondDBtag = "sim-20231017-vc-md100")
# Upgrade DBs

CondDB().Upgrade = True
appMgr = AppMgr()
# Initialize the ApplicationMgr
appMgr.initialize()
# Get some services
TES     = appMgr.evtSvc()
toolSvc = appMgr.toolSvc()
LHCb = gbl.LHCb
propagator = toolSvc.create('TrackRungeKuttaExtrapolator',
                            interface='ITrackExtrapolator')

import itertools
import random
from array import array
from ROOT import TFile, TTree
from ROOT import gROOT, AddressOf
N = 50
f     = TFile('toy_tracks_magdown_allUTLayersStore_Chebychev_NGenPoints_{}_rejectionUT.root'.format(N),'recreate')
ttree = TTree('toyTracks','toyTracks')
ETA   = array( 'f', [ 0 ] )
PHI   = array( 'f', [ 0 ] )
PT    = array( 'f', [ 0 ] )
P     = array( 'f', [ 0 ] )
QOP   = array( 'f', [ 0 ] )
TX    = array( 'f', [0])
TY    = array( 'f', [0])
# https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/TrackEvent/include/Event/StateParameters.h 
# origin, endVelo, endUT, zMagKick, entryT, endT
"""
z = 2304.75
z = 2310.25
z = 2324.75
z = 2330.25
z = 2359.75
z = 2365.25
z = 2379.75
z = 2385.25
z = 2584.75
z = 2590.25
z = 2604.75
z = 2610.25
z = 2639.75
z = 2645.25
z = 2659.75
z = 2665.25
"""

"""
Run3 UT
+-------+--------------------+
| index |    z [mm] value    |
+-------+--------------------+
|   0   |      2306.975      |
|   1   |      2313.025      |
|   2   | 2321.8250000000003 |
|   3   |      2328.425      |
|   4   |      2361.975      |
|   5   |      2368.025      |
|   6   | 2376.8250000000003 |
|   7   |      2383.425      |
|   8   |      2586.925      |
|   9   |      2592.975      |
|   10  |      2601.775      |
|   11  |      2608.375      |
|   12  |      2641.925      |
|   13  |      2647.975      |
|   14  |      2656.775      |
|   15  |      2663.375      |
+-------+--------------------+      
"""

z_StoringState = [ 770. , 
                  2307., 2313. , 2322., 2328.,  
                  2362., 2368. , 2377., 2383., 
                  2586., 2592. , 2601., 2608., 
                  2641., 2647. , 2656., 2663.,
                  5240. , 7500., 8520. ,9410.]

states_x  = ROOT.std.vector("float")( len(z_StoringState))
states_y  = ROOT.std.vector("float")( len(z_StoringState))
states_z  = ROOT.std.vector("float")( len(z_StoringState))
states_tx = ROOT.std.vector("float")( len(z_StoringState))
states_ty = ROOT.std.vector("float")( len(z_StoringState))

states_x.resize( len(z_StoringState))
states_y.resize( len(z_StoringState))
states_z.resize( len(z_StoringState))
states_tx.resize( len(z_StoringState))
states_ty.resize( len(z_StoringState))

ttree.Branch("p",   P   , 'p/F')
ttree.Branch("qop", QOP , 'qop/F')
ttree.Branch("x_Location" ,    states_x )
ttree.Branch("y_Location" ,    states_y )
ttree.Branch("z_Location" ,    states_z )
ttree.Branch("tx_Location",    states_tx)
ttree.Branch("ty_Location",    states_ty)
nProcessed = 0
outAcc =0
idx_c = 1

import math
def generate( k , xmax, xmin , NPoints) : 
    return (xmax+xmin)/2. + (xmax-xmin)/2. * math.cos( math.pi *( 2*k+1) / (2 * NPoints))
n = N
xmin,xmax    =  0       , 3.2e3
ymin,ymax    =  0       , 2.8e3
txmin,txmax  = -0.8     , 0.8
tymin,tymax  = -0.4     , 0.4
qopmin,qopmax=-0.002   , 0.002
xvals   = sorted(  [generate( k , xmax , xmin ,   n) for k in range(0,n) ] + [ -generate( k, xmax, xmin, n) for k in range(0,n)]   ) 
yvals   = sorted(  [generate( k , ymax , ymin ,   n) for k in range(0,n) ] + [ -generate( k, xmax, xmin, n) for k in range(0,n)]   )
txvals  = sorted(  [generate( k , txmax, txmin,   n) for k in range(0,n) ])
tyvals  = sorted(  [generate( k , tymax, tymin,   n) for k in range(0,n) ])
qopvals = sorted(  [generate( k , qopmax, qopmin, n) for k in range(0,n) ])

"""
# Assuming total_iterations is the total number of iterations in your loop
total_iterations = 100
# Placeholder for storing the start time of the loop
start_time = time.time()

# Loop through iterations
for i in range(total_iterations):
    # Your loop's code here
    
    # Placeholder for simulating work
    time.sleep(0.1)  # Simulate some work taking 0.1 seconds
    
    # Calculate progress
    progress = i / total_iterations
    remaining_iterations = total_iterations - i
    
    # Estimate time to completion
    elapsed_time = time.time() - start_time
    avg_time_per_iteration = elapsed_time / (i + 1)
    time_to_end = remaining_iterations * avg_time_per_iteration
    
    # Print progress bar
    bar_length = 50
    progress_bar = '[' + '=' * int(progress * bar_length) + ' ' * (bar_length - int(progress * bar_length)) + ']'
    print(f'\rProgress: {progress_bar} {progress * 100:.2f}% | Estimated Time to End: {time_to_end:.2f} seconds', end='', flush=True)
"""    

total_iterations = len(xvals )*len(yvals)*len(txvals)*len(tyvals)*len(qopvals)

i = 0 
start_time = time.time()
for x,y,tx,ty,qop in itertools.product( xvals, yvals, txvals,tyvals, qopvals):
    #small progress bar
    progress             = float(i/total_iterations)
    remaining_iterations = total_iterations-i
    elapsed_time = time.time() - start_time
    avg_time_per_iteration = elapsed_time / (i + 1)
    time_to_end = remaining_iterations * avg_time_per_iteration
    bar_length = 50
    progress_bar = '[' + '=' * int(progress * bar_length) + ' ' * (bar_length - int(progress * bar_length)) + ']'
    sys.stdout.write('\rProgress: {} {:.2f}% | Estimated Time to End: {:.2f} minutes'.format(progress_bar, progress * 100, time_to_end/60.))
    sys.stdout.flush()
    i+=1 

    charge =  np.sign(qop) 
    QOP[0] = qop
    P[0]   = np.fabs(1./QOP[0])
    track_v_endT   =  track_vector2( x, y, tx, ty, math.fabs(1./qop) , charge )

    states_z.clear()
    states_x.clear()
    states_y.clear()
    states_tx.clear()
    states_ty.clear()

    xat0 = x- track_v_endT[0]*z_StoringState[-1]
    yat0 = y- track_v_endT[0]*z_StoringState[-1]
    
    if math.fabs(yat0) > 600   : continue
    
    #if math.fabs(xat0) > 100  : continue
    states_z.resize( len(z_StoringState))
    states_x.resize( len(z_StoringState))
    states_y.resize( len(z_StoringState))
    states_tx.resize( len(z_StoringState))
    states_ty.resize( len(z_StoringState))
    n = len(z_StoringState)
    track_v = track_v_endT
    z_old   = z_StoringState[-1]
    continueIt = False
    for idx, z_target in enumerate(reversed(z_StoringState)):
        idx_fill = n - idx - 1
        z_original = z_StoringState[idx_fill]
        propagator.propagate( track_v, z_old, z_target)

        if z_target > 2000 and z_target < 3000 and (math.fabs(track_v[0])> 1400  or (math.fabs(track_v[1])>1400)) :
            continueIt = True
            break
        z_old  = z_target
        states_z[idx_fill] = z_target
        states_x[idx_fill] = track_v[0]
        states_y[idx_fill] = track_v[1]
        states_tx[idx_fill]= track_v[2]
        states_ty[idx_fill]= track_v[3]    
    if math.sqrt(states_x.at(0) **2 + states_y.at(0) **2) > 400. or continueIt :
        outAcc +=1
        continue
    ttree.Fill()
    nProcessed += 1
f.Write()
f.Close()
print("DONE")
print("OutAcc {0}".format( outAcc/nProcessed))
