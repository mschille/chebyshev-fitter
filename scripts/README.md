# scripts to produce tuples for the state propagator approximations:
## available scripts
* ```toy_tracks.py```: generates tuples with states in origin, flat in tx, ty,
  q/p, propagated to various z
* ```toy_tracks_chebychev.py```: generates tuples with states at zEndT, with
  Chebyshev-based point distribution in x, y, tx, ty, q/p, propagated to
  various z
* ```toy_tracks_experimental.py```:  Manuel is playing with this at the moment

## running the scripts
To run those scripts, you will need a working LHCb software installation. Then you can just use:

``` sh
lb-run Moore/v50r0 python -O the_file.py
```
