# Copyright (C) CERN for the benefit of the LHCb collaboration
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#####
# Authors : Roel Aaij, Renato Quagliani
# lb-run Brunel/latest python makeToyTracks.py
# lb-run Moore/v50r0  python toy_tracks.py [ should work ]
# Edit your favourite rangees to generate things
####
from __future__ import print_function
import math
#import progressbar
import numpy as np
# Phase 2: GaudiPython
from GaudiPython.Bindings import gbl, AppMgr
from ROOT import gInterpreter, Math, TLorentzVector
from math import cosh, pi
import ROOT
from Configurables import LHCbApp, CondDB
#from LHCb import ParticleID
# Get access to units
gInterpreter.Declare('#include <GaudiKernel/SystemOfUnits.h>')
Units = gbl.Gaudi.Units
import math 
def track_vector2(x, y, tx, ty, p, charge):   
    m = 105.66
    pz = p / math.sqrt( 1. + tx**2 + ty**2)
    E = math.sqrt( p**2 + m**2)
    v = TLorentzVector( tx*pz , ty*pz, pz , E)
    return gbl.Gaudi.TrackVector(x, y, tx, ty, charge / p)
app = LHCbApp(
    DataType="Upgrade",
    EvtMax=50,
    Simulation=True,
    DDDBtag   = "dddb-20231017",
    CondDBtag = "sim-20231017-vc-md100")
# Upgrade DBs
CondDB().Upgrade = True
appMgr = AppMgr()
# Initialize the ApplicationMgr
appMgr.initialize()
# Get some services
TES     = appMgr.evtSvc()
toolSvc = appMgr.toolSvc()
LHCb = gbl.LHCb
propagator = toolSvc.create('TrackRungeKuttaExtrapolator',
                            interface='ITrackExtrapolator')
#origin parameters
x_origin = 0.*Units.mm
y_origin = 0.*Units.mm
z_origin = 0.*Units.mm
import itertools
import random
from array import array
from ROOT import TFile, TTree
from ROOT import gROOT, AddressOf
f     = TFile('toy_tracks_magdown_allUTLayersStore.root','recreate')
ttree = TTree('toyTracks','toyTracks')
ETA   = array( 'f', [ 0 ] )
PHI   = array( 'f', [ 0 ] )
PT    = array( 'f', [ 0 ] )
P     = array( 'f', [ 0 ] )
QOP   = array( 'f', [ 0 ] )
TX    = array( 'f', [0])
TY    = array( 'f', [0])
# https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/TrackEvent/include/Event/StateParameters.h 
# origin, endVelo, endUT, zMagKick, entryT, endT
"""
z = 2304.75
z = 2310.25
z = 2324.75
z = 2330.25
z = 2359.75
z = 2365.25
z = 2379.75
z = 2385.25
z = 2584.75
z = 2590.25
z = 2604.75
z = 2610.25
z = 2639.75
z = 2645.25
z = 2659.75
z = 2665.25
"""

"""
Run3 UT
+-------+--------------------+
| index |    z [mm] value    |
+-------+--------------------+
|   0   |      2306.975      |
|   1   |      2313.025      |
|   2   | 2321.8250000000003 |
|   3   |      2328.425      |
|   4   |      2361.975      |
|   5   |      2368.025      |
|   6   | 2376.8250000000003 |
|   7   |      2383.425      |
|   8   |      2586.925      |
|   9   |      2592.975      |
|   10  |      2601.775      |
|   11  |      2608.375      |
|   12  |      2641.925      |
|   13  |      2647.975      |
|   14  |      2656.775      |
|   15  |      2663.375      |
+-------+--------------------+      
"""
# z_StoringState = [ 770. , 2320., 2650. , 5240. , 7500., 8520. ,9410.]

z_StoringState = [ 770. , 
                  2307., 2313. , 2322., 2328.,  
                  2362., 2368. , 2377., 2383., 
                  2586., 2592. , 2601., 2608., 
                  2641., 2647. , 2656., 2663.,
                  5240. , 7500., 8520. ,9410.]

states_x  = ROOT.std.vector("float")( len(z_StoringState))
states_y  = ROOT.std.vector("float")( len(z_StoringState))
states_z  = ROOT.std.vector("float")( len(z_StoringState))
states_tx = ROOT.std.vector("float")( len(z_StoringState))
states_ty = ROOT.std.vector("float")( len(z_StoringState))

states_x.resize( len(z_StoringState))
states_y.resize( len(z_StoringState))
states_z.resize( len(z_StoringState))
states_tx.resize( len(z_StoringState))
states_ty.resize( len(z_StoringState))

ttree.Branch("eta", ETA , 'eta/F')
ttree.Branch("phi", PHI , 'phi/F')
ttree.Branch("pt",  PT  , 'pt/F')
ttree.Branch("txO", TX  , 'txO/F')
ttree.Branch("tyO", TY  , 'tyO/F')
ttree.Branch("p",   P   , 'p/F')
ttree.Branch("qop", QOP , 'qop/F')
ttree.Branch("x_Location" ,     states_x)
ttree.Branch("y_Location" ,     states_y)
ttree.Branch("z_Location" ,     states_z)
ttree.Branch("tx_Location",    states_tx)
ttree.Branch("ty_Location",    states_ty)
nProcessed = 0
outAcc =0
idx_c = 1
nStepsTX = 100
nStepsTY = 100
nStepsP  = 200
tx_origin_values = np.linspace(-0.4,0.4, num = nStepsTX)
ty_origin_values = np.linspace(-0.4,0.4, num = nStepsTY)
qop_values  = np.linspace( 1./(100*Units.GeV) , 1./(0.5*Units.GeV), num =  nStepsP )
toProcess   =  len(tx_origin_values)*len(ty_origin_values)*len(qop_values)
for tx, ty , qop in itertools.product( tx_origin_values, ty_origin_values, qop_values):
    for chargeuse in [-1.,1.]:
        charge =  chargeuse
        QOP[0] = qop*charge
        P[0]   = 1./QOP[0]
        track_v_origin = track_vector2( x_origin, y_origin, tx, ty, 1./qop , charge )
        TX[0]   = tx
        TY[0]   = ty
        PX = tx* math.fabs(P[0])
        PY = ty* math.fabs(P[0])
        PZ = math.sqrt( P[0]*P[0] - PX*PX - PY*PY)
        PT[0]  =  math.sqrt( PX**2 + PY**2)
        #print("----- track base generated ( mass, charge,  p , eta, phi ) = {0} , {1} , {2} , {3} , {4}".format(  pion_or_kaon, charge, momentum, eta, phi))
        z_old = z_origin
        track_v = track_v_origin
        states_z.clear()
        states_x.clear()
        states_y.clear()
        states_tx.clear()
        states_ty.clear()
        
        states_z.resize( len(z_StoringState))
        states_x.resize( len(z_StoringState))
        states_y.resize( len(z_StoringState))
        states_tx.resize( len(z_StoringState))
        states_ty.resize( len(z_StoringState))
        
        for idx,z_target in enumerate(z_StoringState) :
            propagator.propagate(track_v, z_old, z_target)
            z_old  = z_target
            states_z[idx] = z_target
            states_x[idx] = track_v[0]
            states_y[idx] = track_v[1] 
            states_tx[idx]= track_v[2]
            states_ty[idx]= track_v[3]
        # reject out of acceptance tracks 
        if math.fabs(states_x.at(len(z_StoringState)-1) )> 3000*Units.mm  or math.fabs(states_y.at(len(z_StoringState)-1))>2700*Units.mm :
            outAcc +=1
            continue
        #for x,y,z in zip( states_x,states_y, states_z):
        #    print(x,y,z)
        ttree.Fill()
        nProcessed += 1
f.Write()
f.Close()
print("DONE")
print("OutAcc {0}".format( outAcc/nProcessed))
