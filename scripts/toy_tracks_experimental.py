# Copyright (C) CERN for the benefit of the LHCb collaboration
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#####
# Authors : Roel Aaij, Renato Quagliani
# 2024-03-03 M.Schiller heavy modification, don't blame Roel or Renato
# lb-run Brunel/latest python makeToyTracks.py
# lb-run Moore/v50r0  python toy_tracks.py [ should work ]
# Edit your favourite rangees to generate things
####
from __future__ import print_function
import math
import sys
import time

import numpy as np
# Phase 2: GaudiPython
from GaudiPython.Bindings import gbl, AppMgr
from ROOT import gInterpreter, Math, TLorentzVector
from math import cosh, pi
import ROOT
from Configurables import LHCbApp, CondDB
#from LHCb import ParticleID
# Get access to units
gInterpreter.Declare('#include <GaudiKernel/SystemOfUnits.h>')
Units = gbl.Gaudi.Units
import math
app = LHCbApp(
    DataType="Upgrade",
    EvtMax=50,
    Simulation=True,
    DDDBtag   = "dddb-20231017",
    CondDBtag = "sim-20231017-vc-md100")
# Upgrade DBs

CondDB().Upgrade = True
appMgr = AppMgr()
# Initialize the ApplicationMgr
appMgr.initialize()
# Get some services
TES     = appMgr.evtSvc()
toolSvc = appMgr.toolSvc()
LHCb = gbl.LHCb
propagator = toolSvc.create('TrackRungeKuttaExtrapolator',
                            interface='ITrackExtrapolator')

import itertools
import random
from array import array
from ROOT import TFile, TTree
from ROOT import gROOT, AddressOf
# https://gitlab.cern.ch/lhcb/LHCb/-/blob/master/Event/TrackEvent/include/Event/StateParameters.h 
# origin, endVelo, endUT, zMagKick, entryT, endT
"""
z = 2304.75
z = 2310.25
z = 2324.75
z = 2330.25
z = 2359.75
z = 2365.25
z = 2379.75
z = 2385.25
z = 2584.75
z = 2590.25
z = 2604.75
z = 2610.25
z = 2639.75
z = 2645.25
z = 2659.75
z = 2665.25
"""

"""
Run3 UT
+-------+--------------------+
| index |    z [mm] value    |
+-------+--------------------+
|   0   |      2306.975      |
|   1   |      2313.025      |
|   2   | 2321.8250000000003 |
|   3   |      2328.425      |
|   4   |      2361.975      |
|   5   |      2368.025      |
|   6   | 2376.8250000000003 |
|   7   |      2383.425      |
|   8   |      2586.925      |
|   9   |      2592.975      |
|   10  |      2601.775      |
|   11  |      2608.375      |
|   12  |      2641.925      |
|   13  |      2647.975      |
|   14  |      2656.775      |
|   15  |      2663.375      |
+-------+--------------------+      
"""

z_StoringState = [770.,
                  2307., 2313., 2322., 2328.,
                  2362., 2368., 2377., 2383.,
                  2586., 2592., 2601., 2608.,
                  2641., 2647., 2656., 2663.,
                  5240., 7500., 8520., 9410.]
maxXY = [(600., 600.),
         (1400., 1400.), (1400., 1400.), (1400., 1400.), (1400., 1400.),
         (1400., 1400.), (1400., 1400.), (1400., 1400.), (1400., 1400.),
         (1400., 1400.), (1400., 1400.), (1400., 1400.), (1400., 1400.),
         (1400., 1400.), (1400., 1400.), (1400., 1400.), (1400., 1400.),
         (5000., 3000.),
         (3000., 3000.), (3000., 3000.), (3000., 3000.)]
nz = len(z_StoringState)

nProcessed = 0
outAcc = 0


def generate( k , xmax, xmin , NPoints) :
    return float(k)/float(NPoints) * xmin + float(NPoints - 1 -
                                                  k)/float(NPoints) * xmax
    return (xmax+xmin)/2. + (xmax-xmin)/2. * math.cos( math.pi *( 2*k+1) / (2 * NPoints))


def generate_qop(pmin, pcut, n):
    """
    generate n q/p values (with both signs of q), 3/4 of samples between
    pmin and pcut flat in p, the remaining samples flat in q/p
    """
    def flat_in_p(pmin, pcut, n):
        return (
            [ 1. / (pcut * float(k) / float(n - 1) + pmin * float(n - 1 - k) /
                    float(n - 1)) for k in range(0, n) ] +
            [ -1. / (pcut * float(k) / float(n - 1) + pmin * float(n - 1 - k) /
                    float(n - 1)) for k in range(0, n) ])
    def flat_in_qop(pcut, n):
        return (float(k) / float(n + 1) / pcut - float(n + 1 - k) /
                float(n + 1) / pcut for k in range(1, n + 1))
    pvals = flat_in_p(pmin, pcut, int(3 * n / 8)) + [
        qop for qop in flat_in_qop(pcut, n - 2 * int(3 * n / 8))]
    return sorted(pvals)


N = 16
nx = N
ny = N
ntx = N
nty = N
nqop = 8 * N
xmin, xmax = 0, 3.2e3
ymin, ymax = 0, 2.8e3
txmin, txmax = -0.8, 0.8
tymin, tymax = -0.5, 0.5
qopmin, qopmax = -0.002, 0.002
xvals   = sorted([ generate( k,   xmax,   xmin, nx  ) for k in range(0, nx  ) ] +
                 [-generate( k,   xmax,   xmin, nx  ) for k in range(0, nx  ) ])
yvals   = sorted([ generate( k,   ymax,   ymin, ny  ) for k in range(0, ny  ) ] +
                 [-generate( k,   ymax,   ymin, ny  ) for k in range(0, ny  ) ])
qopvals = generate_qop(500., 10000., nqop)

f = TFile('toy_tracks_magdown_allUTLayersStore_Chebychev_NGenPoints_{}_rejectionUT.root'.format(N), 'recreate')
ttree = TTree('toyTracks', 'toyTracks')

QOP = array('f', [0])
states_x = ROOT.std.vector("float")(len(z_StoringState))
states_y = ROOT.std.vector("float")(len(z_StoringState))
states_z = ROOT.std.vector("float")(len(z_StoringState))
states_tx = ROOT.std.vector("float")(len(z_StoringState))
states_ty = ROOT.std.vector("float")(len(z_StoringState))

ttree.Branch("qop", QOP, 'qop/F')
ttree.Branch("x_Location", states_x)
ttree.Branch("y_Location", states_y)
ttree.Branch("z_Location", states_z)
ttree.Branch("tx_Location", states_tx)
ttree.Branch("ty_Location", states_ty)

total_iterations = 2 * nx * 2 * ny * ntx * nty * nqop


def sloperange(x, z, xmin, xmax, zref=9410.):
    tx1, tx2 = (xmin - x) / (z - zref), (xmax - x) / (z - zref)
    return (min(tx1, tx2), max(tx1, tx2))


def intersect_range(r1, r2):
    return (max(r1[0], r2[0]), min(r1[1], r2[1]))


for i in range(0, nz):
    states_z[i] = z_StoringState[i]
i = 0
start_time = time.time()
for x, y, qop in itertools.product(xvals, yvals, qopvals):
    tyrange = intersect_range((tymin, tymax), ((y / 9410. - 0.2), (y / 9410. + 0.2)))
    tyrange = intersect_range(tyrange, sloperange(y, 0., -600., 600.))
    tyrange = intersect_range(tyrange, sloperange(y, 7500., -1.05 * ymax, 1.05 * ymax))
    tyrange = intersect_range(tyrange, sloperange(y, 5420., -6000., 6000.))
    txrange = intersect_range((txmin, txmax), sloperange(x, 7500., -1.05 * xmax, 1.05 * xmax))
    txrange = intersect_range(txrange, sloperange(x, 5420., -6000., 6000.))
    QOP[0] = qop
    states_x[nz - 1] = x
    states_y[nz - 1] = y
    for tx in filter(lambda tx: (abs(tx) < 0.2 or abs(qop) >= 1.4e-3 or
                                 abs(tx - 800. * qop) <= 0.6),
                     (generate(k, txrange[0], txrange[1], ntx) for k in
                      range(0, ntx))):
        states_tx[nz - 1] = tx
        for ty in (generate(k, tyrange[0], tyrange[1], nty) for k in range(0, nty)):
            #small progress bar
            if 0 == (i & 65535):
                progress             = float(i)/total_iterations
                remaining_iterations = total_iterations-i
                elapsed_time = time.time() - start_time
                avg_time_per_iteration = elapsed_time / float(i + 1)
                time_to_end = remaining_iterations * avg_time_per_iteration
                bar_length = 50
                progress_bar = '[' + '=' * int(progress * bar_length) + ' ' * (bar_length - int(progress * bar_length)) + ']'
                sys.stdout.write('\rProgress: {} {:6.2f}% | ETA {:.1f} minutes'.format(progress_bar, progress * 100, time_to_end/60.))
                sys.stdout.flush()
            i += 1

            states_ty[nz - 1] = ty
            track_v = gbl.Gaudi.TrackVector(x, y, tx, ty, qop)
            z_old = z_StoringState[nz - 1]
            for idx in range(nz - 2, -1, -1):
                z_target = z_StoringState[idx]

                sc = propagator.propagate(track_v, z_old, z_target)

                if (sc.isFailure() or
                    max(abs(track_v[2]), abs(track_v[3])) > 5. or
                    abs(track_v[0]) > maxXY[idx][0] or
                    abs(track_v[1]) > maxXY[idx][1]):
                    break
                z_old = z_target
                states_z[idx] = z_target
                states_x[idx] = track_v[0]
                states_y[idx] = track_v[1]
                states_tx[idx] = track_v[2]
                states_ty[idx] = track_v[3]
            if z_old != z_StoringState[0]:
                outAcc += 1
                continue
            ttree.Fill()
            nProcessed += 1
f.Write()
f.Close()
print("DONE")
print("OutAcc {0}".format(float(outAcc)/nProcessed))
