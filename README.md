# chebyshev-fitter

The ```chebyshev-fitter``` package is a package that allows you to do fits that
a linear in model parameters, with an emphasis on multi-dimensional Chebyshev
approximations. The package is fairly general, but examples are currently
limited to fitting approximations of propagating track state vectors through
LHCb's magnetic field.

## requirements
You will need a C++20 compiler, and a recent version of ROOT.

## building
Building is simple:

``` sh
make
```

## unit tests
The code contains some unit tests to check the functionality of the basic
classes, you can run them like this:

``` sh
make test
```

This will compile and run the unit tests, and deposit the test results in the
testresults subdirectory.

## cleaning up
The Makefile supports ```make clean``` and ```make distclean```. The former
cleans up object files, the latter also cleans all binaries and test results.

## documentation
For now, the documentation is in the source files in the form of doxygen
comments. Better materials will appear in the near future.

## directory structure
* ```include```: include files
* ```tests```: unit tests
* ```testresults```: results of unit tests
* ```scripts```: contains python code to generate tuples with track states
  propagated through the magnetic field
* ```statepropfitter```: contains code to fit those tuples, and produces plots
  and code that implements the fitted approximations

## contact
Please feel free to contact the author(s) to ask questions, ask for features,
or provide fixes or new code.
