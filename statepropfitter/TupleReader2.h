/* Copyright (C) CERN for the benefit of the LHCb collaboration
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * In applying this licence, CERN does not waive the privileges and immunities
 * granted to it by virtue of its status as an Intergovernmental Organization
 * or submit itself to any jurisdiction.
 */
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Mar  2 12:28:46 2024 by ROOT version 6.31/01
// from TTree toyTracks/toyTracks
// found on file: toy_tracks_magdown_allUTLayersStore_Chebychev_NGenPoints_50_rejectionUT.root
//
// Hacked further by M. Schiller 2024-03-07
//////////////////////////////////////////////////////////
#pragma once

#include <vector>

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

class TupleReader2 {
public:
    TTree* fChain;  //! pointer to the analyzed TTree or TChain
    Int_t fCurrent; //! current Tree number in a TChain

    // Fixed size dimensions of array or collections stored in the TTree if
    // any.

    // Declaration of leaf types
    Float_t qop;
    std::vector<float>* x_Location;
    std::vector<float>* y_Location;
    std::vector<float>* z_Location;
    std::vector<float>* tx_Location;
    std::vector<float>* ty_Location;

    // List of branches
    TBranch* b_qop;         //!
    TBranch* b_x_Location;  //!
    TBranch* b_y_Location;  //!
    TBranch* b_z_Location;  //!
    TBranch* b_tx_Location; //!
    TBranch* b_ty_Location; //!

    TupleReader2(const char* fname = nullptr, const char* treename = nullptr);
    ~TupleReader2();
    Int_t Cut(Long64_t entry);
    Int_t GetEntry(Long64_t entry);
    Long64_t LoadTree(Long64_t entry);
    void Init(TTree* tree);
    void Loop();
    bool Notify();
    void Show(Long64_t entry = -1);
};

// vim: sw=4:tw=78:ft=cpp:et
